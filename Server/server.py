# forked from https: // github.com/E-Renshaw/ftp-socket-server-python
import click
from socket import AF_INET, socket, SOCK_STREAM
import sys
import time
import os
import struct
import argparse
from threading import Thread


TCP_IP = "127.0.0.1"
BUFFER_SIZE = 1024
client_address = {}
addresses = {}
clients = {}
FTP_DIR = "server_files/"
os.chdir(FTP_DIR)


def list_files(client):
    print("Listing files...")
    listing = os.listdir(os.getcwd())
    client.send(struct.pack("i", len(listing)))
    total_directory_size = 0
    for i in listing:
        print(i)
        client.send(bytes(struct.pack("i", sys.getsizeof(i))))
        time.sleep(0.0001)
        client.send(bytes(i, "utf8"))
        time.sleep(0.0001)
        client.send(bytes(struct.pack("i", os.path.getsize(i))))
        time.sleep(0.0001)
        total_directory_size += os.path.getsize(i)
    client.send(bytes(struct.pack("i", total_directory_size)))
    print("Successfully sent file listing")
    return


def download_file(client):
    print("Downloading file...")
    client.send(bytes("OK", "utf8"))
    file_name_length = struct.unpack("h", client.recv(2))[0]
    print(file_name_length)
    file_name = client.recv(file_name_length)
    print(file_name)
    if os.path.isfile(file_name):
        client.send(bytes(struct.pack("i", os.path.getsize(file_name))))
    else:
        print("File name not valid")
        client.send(bytes(struct.pack("i", -1)))
        return
    client.recv(BUFFER_SIZE)
    start_time = time.time()
    print("Sending file...")
    content = open(file_name, "rb")
    l = content.read(BUFFER_SIZE)
    while l:
        client.send(bytes(l))
        l = content.read(BUFFER_SIZE)
    content.close()
    client.recv(BUFFER_SIZE)
    client.send(bytes(struct.pack("f", time.time() - start_time)))
    return


def delete_file(client):
    client.send(bytes("1", "utf8"))
    file_name_length = struct.unpack("h", client.recv(2))[0]
    file_name = client.recv(file_name_length).decode('utf8')
    print("trying to delete : ", file_name)
    if os.path.isfile(file_name):
        print("file exist")
        client.send(bytes(struct.pack("i", 1)))
    else:
        print('file not exist')
        client.send(bytes(struct.pack("i", -1)))
        return
    confirm_delete = client.recv(BUFFER_SIZE).decode('utf8')
    if confirm_delete == "Y":
        try:
            os.remove(file_name)
            client.send(bytes(struct.pack("i", 1)))
        except Exception as e:
            print(F"Failed to delete {file_name} : {e}")
            client.send(bytes(struct.pack("i", -1)))
    else:
        print("Delete abandoned by client!")
        return


def quit(client):
    client.send(bytes("1", "utf8"))
    client.close()
    del clients[client]


def handle_client(client):
    while True:
        print("\n\nWaiting for instruction")
        data = client.recv(BUFFER_SIZE).decode("utf8")
        print(F"\nRecieved instruction: {data}")
        if data == "LIST":
            list_files(client)
        elif data == "DWLD":
            download_file(client)
        elif data == "DELF":
            delete_file(client)
        elif data == "QUIT":
            quit(client)
            break
        data = None


def accept_incoming_connections(s):
    while True:
        client, client_address = s.accept()
        print("%s:%s has connected." % client_address)
        clients[client] = client_address
        Thread(target=handle_client, args=(client,)).start()


@click.command()
@click.option('-sp', default=8000, help='Port')
def main(sp):
    print("\nWelcome to the FTP server.")
    addr = (TCP_IP, sp)
    server = socket(AF_INET, SOCK_STREAM)
    server.bind(addr)
    server.listen(5)
    print(F"Server Initialized on port {sp}")
    accept_thread = Thread(target=accept_incoming_connections, args=(server,))
    accept_thread.start()
    accept_thread.join()
    server.close()


if __name__ == "__main__":
    main()
