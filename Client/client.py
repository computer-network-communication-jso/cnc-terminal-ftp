# forked from https: // github.com/E-Renshaw/ftp-socket-server-python
import click
from socket import AF_INET, socket, SOCK_STREAM
import sys
import os
import struct
import argparse
from threading import Thread

BUFFER_SIZE = 1024
FILE_DIR = 'client_files/'


def conn(s, sip, sp):
    print("Sending server request...")
    try:
        s.connect((sip, sp))
        print("Connection sucessful")
    except:
        print("Connection unsucessful. Make sure the server is online.")


def list_files(s):
    print("Requesting files...\n")
    try:
        s.send(bytes("LIST", "utf8"))
    except Exception as e:
        print(e)
        print("Couldn't make server request. Make sure a connection has bene established.")
        return
    try:
        number_of_files = struct.unpack("i", s.recv(4))[0]
        print(F"Number of files : {number_of_files}")
        for i in range(int(number_of_files)):
            file_name_size = struct.unpack("i", s.recv(4))[0]
            file_name = s.recv(file_name_size)
            file_size = struct.unpack("i", s.recv(4))[0]
            print(F"\t{i} - {file_name.decode('utf8')} - {file_size}b")
        total_directory_size = struct.unpack("i", s.recv(4))[0]
        print(F"Total directory size: {total_directory_size}b")
    except Exception as e:
        print(e)
        print("Couldn't retrieve listing")
        return


def download_file(s, file_name):
    # Download given file
    print(F"Downloading file: {file_name}")
    try:
        # Send server request
        s.send(bytes("DWLD", "utf8"))
    except:
        print("Couldn't make server request. Make sure a connection has bene established.")
        return
    try:
        # Wait for server ok, then make sure file exists
        s.recv(BUFFER_SIZE)
        # Send file name length, then name
        s.send(bytes(struct.pack("h", sys.getsizeof(file_name))))
        s.send(bytes(file_name, "utf8"))
        # Get file size (if exists)
        file_size = struct.unpack("i", s.recv(4))[0]
        if file_size == -1:
            # If file size is -1, the file does not exist
            print("File does not exist. Make sure the name was entered correctly")
            return
    except:
        print("Error checking file")
    try:
        # Send ok to recieve file content
        s.send(bytes("sync", "utf8"))
        # Enter loop to recieve file
        output_file = open(FILE_DIR+file_name, "wb")
        bytes_recieved = 0
        print("\nDownloading...")
        while bytes_recieved < file_size:
            # Again, file broken into chunks defined by the BUFFER_SIZE variable
            l = s.recv(BUFFER_SIZE)
            output_file.write(l)
            bytes_recieved += BUFFER_SIZE
        output_file.close()
        print(F"Successfully downloaded {file_name}")
        # Tell the server that the client is ready to recieve the download performance details
        s.send(bytes("sync", "utf8"))
        # Get performance details
        time_elapsed = struct.unpack("f", s.recv(4))[0]
        print(F"Time elapsed: {time_elapsed}s\nFile size: {file_size}b")
    except:
        print("Error downloading file")
        return
    return


def delete_file(s, file_name):
    # Delete specified file from file server
    print(F"Deleting file: {file_name}...")
    try:
        s.send(bytes("DELF", "utf8"))
        s.recv(BUFFER_SIZE)
    except:
        print("Couldn't connect to server. Make sure a connection has been established.")
        return
    try:
        s.send(bytes(struct.pack("h", sys.getsizeof(file_name))))
        s.send(bytes(file_name, "utf8"))
    except:
        print("Couldn't send file details")
        return
    try:
        # Get conformation that file does/doesn't exist
        file_exists = struct.unpack("i", s.recv(4))[0]
        if file_exists == -1:
            print("The file does not exist on server")
            return
    except:
        print("Couldn't determine file existance")
        return
    try:
        confirm_delete = input(
            F"Are you sure you want to delete {file_name}? (Y/N)\n").upper()
        while confirm_delete != "Y" and confirm_delete != "N" and confirm_delete != "YES" and confirm_delete != "NO":
            print("Command not recognised, try again")
            confirm_delete = input(
                F"Are you sure you want to delete {file_name}? (Y/N)\n").upper()
    except:
        print("Couldn't confirm deletion status")
        return
    try:
        if confirm_delete == "Y" or confirm_delete == "YES":
            s.send(bytes("Y", "utf8"))
            delete_status = struct.unpack("i", s.recv(4))[0]
            if delete_status == 1:
                print("File successfully deleted")
                return
            else:
                print("File failed to delete")
                return
        else:
            s.send(bytes("N", "utf8"))
            print("Delete abandoned by user!")
            return
    except:
        print("Couldn't delete file")
        return


def quit(s):
    s.send(bytes("QUIT", "utf8"))
    s.recv(BUFFER_SIZE)
    s.close()
    print("Server connection ended")
    return


def handle_requests(client_socket):
    print("\n\nWelcome to the FTP client.\n\nCall one of the following functions:\list\t\t: List files\retrive <file_path> : Download file\delete <file_path> : Delete file\nQuit\t\t: Exit")
    while True:
        prompt = input("\nEnter a command: ").lower()
        print(prompt)
        if prompt[:4] == "list":
            list_files(client_socket)
        elif prompt[:8] == "retrieve":
            download_file(client_socket, prompt[9:])
        elif prompt[:6] == "delete":
            delete_file(client_socket, prompt[7:])
        elif prompt[:4] == "quit":
            quit(client_socket)
            break
        else:
            print("Command not recognised; please try again")


@click.command()
@click.option('-sip', default="localhost", help='Server IP')
@click.option('-sp', default=8000, help='Port')
def main(sip, sp):
    addr = (sip, sp)
    client_socket = socket(AF_INET, SOCK_STREAM)
    conn(client_socket, sip, sp)
    receive_thread = Thread(target=handle_requests, args=(client_socket, ))
    receive_thread.start()


if __name__ == "__main__":
    main()
